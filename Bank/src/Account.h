#pragma once

#include <src/Header.h>

#include "Transference.h"

class Account
{
public:
	static std::vector<Account> s_accounts;
	static std::vector<Transference> s_transferences;

	static int Create();
	static int Create(std::string userName, std::string password, std::string firstName, std::string lastName, Gender gender);
	
	static int SignIn();
	
	static void ShowList();
	static void ShowTransferences(int accountIndex, int lastIndexOfTransference);
	
	static std::string GetMoneyAsText(double money);

public:
	User m_user;
	double m_money;
	int m_lastIndexOfTransference;

	Account(User user)
	{
		m_user = user;
		m_money = 0;
		m_lastIndexOfTransference = 0;
	}

	//actions
	void ShowInformation();
	void EditInformation();
	void ChangePassword();
	void DepositMoney();
	void WithdrawMoney();
	void TransferMoney(int myIndex);
};