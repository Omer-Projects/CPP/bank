#include "Transference.h"

std::string Transference::GetTimeAsText()
{
	tm time{};
	localtime_s(&time, &m_time);

	char buffer[80];
	strftime(buffer, sizeof(buffer), "%d/%m/%Y %H:%M", &time);
	std::string text(buffer);

	return text;
}