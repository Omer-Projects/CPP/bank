#pragma once

#include <src/Header.h>

inline std::tm localtime_xp(std::time_t timer)
{
	std::tm bt{};
	localtime_s(&bt, &timer);
	return bt;
}

struct Transference
{
	std::time_t m_time;
	int m_userFromIndex;
	int m_userToIndex;
	double m_money;

	Transference(int userFromIndex, int userToIndex, double money)
	{
		m_time = std::time(0);
		m_userFromIndex = userFromIndex;
		m_userToIndex = userToIndex;
		m_money = money;
	}

	std::string GetTimeAsText();
};