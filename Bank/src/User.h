#ifndef _USER_H
#define _USER_H

#include <src/Header.h>
#include <algorithm>

struct User
{
	std::string m_userName; // size 4
	std::string m_password; // size 20

	std::string m_firstName; // size 10
	std::string m_lastName; // size 10
	Gender m_gender;

	User() {}

	User(std::string userName, std::string password, std::string firstName, std::string lastName, Gender& gender)
	{
		m_userName = userName;
		m_password = password;
		m_firstName = firstName;
		m_lastName = lastName;
		m_gender = gender;
	}

	std::string GetFullName()
	{
		return m_firstName + " " + m_lastName;
	}
};

#endif