#include "Account.h"

std::vector<Account> Account::s_accounts;
std::vector<Transference> Account::s_transferences;

// Get from the user
static std::string GetText(const char* label, const int length)
{
	do
	{
		std::cout << label << ": ";

		std::string text;
		std::cin >> text;
		if (text.length() == 0)
		{
			std::cout << "The " << label << " can't be empty!\n";
		}
		else if (text.length() > length)
		{
			std::cout << "The " << label << " can't be more than " << length << " litters!\n";
		}
		else
		{
			return text;
		}
	}
	while (true);
}
static int GetUserName(bool withPassword = false)
{
	std::string userName = GetText("User Name", 4);
	std::string password;

	if (withPassword)
	{
		password = GetText("Password", 20);
	}

	userName[0] = toupper(userName[0]);

	int index = 0;
	for (Account& account : Account::s_accounts)
	{
		if (userName.compare(account.m_user.m_userName) == 0 && (!withPassword || password.compare(account.m_user.m_password) == 0))
		{
			return index;
		}
		index++;
	}

	return -1;
}
static void GetAccountInformation(std::string* firstName, std::string* lastName, Gender* gender)
{
	// First Name
	*firstName = GetText("First Name", 10);

	(*firstName)[0] = toupper((*firstName)[0]);

	// Last Name
	*lastName = GetText("Last Name", 10);

	(*lastName)[0] = toupper((*lastName)[0]);

	// Gender
	std::cout << "Gender (1 - Male, 2 - Female, 3 - Other): ";
	unsigned short selectGender;
	std::cin >> selectGender;

	if (selectGender == 1 || selectGender == 2)
	{
		*gender = (Gender)selectGender;
	}
	else
	{
		*gender = Gender::Other;
	}
}
static std::string GetPassword()
{
	do
	{
		std::string password;
		std::string confirmPassword;

		std::cout << "Password: ";
		std::cin >> password;

		std::cout << "Confirm Password: ";
		std::cin >> confirmPassword;

		if (4 > password.length() || password.length() > 20)
		{
			std::cout << "The password can only between 4 to 20 letters!\n";
		}
		else
		{
			bool good = true;
			for (char tv : password)
			{
				good = ('0' <= tv && tv <= '9') || ('a' <= tv && tv <= 'z') || ('A' <= tv && tv <= 'Z');
				if (!good)
				{
					break;
				}
			}

			if (!good)
			{
				std::cout << "The password can only be from English litters and numbers!\n";
				break;
			}

			if (password.compare(confirmPassword) == 0)
			{
				return password;
			}
			else
			{
				std::cout << "The passwords is not identical!\n";
			}
		}
	}
	while (true);
}
static double GetMoney()
{
	double money = 0;

	std::cout << "Money: ";
	std::cin >> money;

	money = round(money * 100) / 100;

	return money;
}

// static
int Account::Create()
{
	int accountsCount = s_accounts.size();
	if (accountsCount == 999)
	{
		std::cout << "Cant create more the 999 accounts!\n";
		return -1;
	}

	std::cout << "Create Account:\n";

	std::string firstName;
	std::string lastName;
	Gender gender;
	std::string password;

	GetAccountInformation(&firstName, &lastName, &gender);

	password = GetPassword();

	// User Name
	std::string id = std::to_string(accountsCount + 1);
	std::string userName = "U";
	for (int i = 3; i > id.length(); i--)
	{
		userName += "0";
	}
	userName += id;

	return Create(userName, password, firstName, lastName, gender);
}

int Account::Create(std::string userName, std::string password, std::string firstName, std::string lastName, Gender gender)
{
	User user(userName, password, firstName, lastName, gender);
	Account newAccount(user);

	s_accounts.push_back(newAccount);
	int index = s_accounts.size() - 1;
	return index;
}

int Account::SignIn()
{
	std::cout << "Sign In:\n";

	int index = GetUserName(true);
	if (index == -1)
	{
		std::cout << "the User Name or Password is incorrect!\n";
	}
	return index;
}

void Account::ShowList()
{
	std::cout << "Exist " << Account::s_accounts.size() << " Accounts:\n";
	for (Account& account : Account::s_accounts)
	{
		std::cout << account.m_user.m_userName << ": \"" << account.m_user.m_firstName << " " << account.m_user.m_lastName << "\" have " << account.m_money << " money.\n";
	}
}

void Account::ShowTransferences(int accountIndex, int lastIndexOfTransference)
{
	std::cout << "Income and Expenses:\n";
	int index = 0;
	for (Transference& transference : Account::s_transferences)
	{
		if (accountIndex == transference.m_userFromIndex || accountIndex == transference.m_userToIndex)
		{
			std::cout << transference.GetTimeAsText() << ": ";
			int otherAccountIndex;
			if (accountIndex == transference.m_userFromIndex)
			{
				std::cout << "Expenses " << Account::GetMoneyAsText(transference.m_money) << " money to ";
				otherAccountIndex = transference.m_userToIndex;
			}
			else
			{
				std::cout << "Income " << Account::GetMoneyAsText(transference.m_money) << " money from ";
				otherAccountIndex = transference.m_userFromIndex;
			}
			
			Account otherAccount = s_accounts[otherAccountIndex];
			std::cout << otherAccount.m_user.GetFullName() << " (" << otherAccount.m_user.m_userName << ").\n";
		}

		index++;
		if (index > lastIndexOfTransference)
		{
			break;
		}
	}
	std::cout << "\n";
}

std::string Account::GetMoneyAsText(double money)
{
	std::string text = std::to_string((int)money);

	int rest = (money - (int)money) * 100;
	if (rest == 0)
	{
		text += ".00";
	}
	else if (rest < 10)
	{
		text += ".0" + std::to_string(rest);
	}
	else
	{
		text += "." + std::to_string(rest);
	}

	return text;
}

// actions
void Account::ShowInformation()
{
	std::cout << "Account Information:\n";
	std::cout << "User Name: " << m_user.m_userName << "\n";
	std::cout << "First Name: " << m_user.m_firstName << "\n";
	std::cout << "Last Name: " << m_user.m_lastName << "\n";
	std::cout << "Gender: ";
	switch (m_user.m_gender)
	{
		case Gender::Other:
		{
			std::cout << "Other\n";
		}
		break;
		case Gender::Male:
		{
			std::cout << "Male\n";
		}
		break;
		case Gender::Female:
		{
			std::cout << "Female\n";
		}
		break;
	}
}

void Account::EditInformation()
{
	std::cout << "Edit Account Information:\n";

	std::string firstName;
	std::string lastName;
	Gender gender;

	GetAccountInformation(&firstName, &lastName, &gender);

	m_user.m_firstName = firstName;
	m_user.m_lastName = lastName;
	m_user.m_gender = gender;
	
	std::cout << "Account Information Saved.\n";
}

void Account::ChangePassword()
{
	std::cout << "Change Password:\n";

	std::string password = GetPassword();

	m_user.m_password = password;

	std::cout << "Password Saved.\n";
}

void Account::DepositMoney()
{
	std::cout << "Deposit Money:\n";

	double money = GetMoney();

	if (money > 0)
	{
		m_money += money;
	}
	else
	{
		std::cout << "You can not deposit negative money!";
	}
}

void Account::WithdrawMoney()
{
	std::cout << "Withdraw Money:\n";

	double money = GetMoney();

	if (money > 0)
	{
		if (money <= m_money)
		{
			m_money -= money;
		}
		else
		{
			std::cout << "You not have " << money << " money.\n";
		}
	}
	else
	{
		std::cout << "You can not withdraw negative money!\n";
	}
}

void Account::TransferMoney(int myIndex)
{
	std::cout << "Transfer Money:\n";

	int targetAccountIndex = GetUserName();
	double money = GetMoney();

	if (targetAccountIndex == -1)
	{
		std::cout << "The User next exist!\n";
	}
	else if (targetAccountIndex == myIndex)
	{
		std::cout << "You can't transfer money to yoursalfe!\n";
	}
	else
	{
		if (money > 0)
		{
			if (money <= m_money)
			{
				m_money -= money;
				s_accounts[targetAccountIndex].m_money += money;

				Transference transference(myIndex, targetAccountIndex, money);
				s_transferences.push_back(transference);

				m_lastIndexOfTransference = s_transferences.size() - 1;

				std::cout << "The money transfer is complete.\n";
			}
			else
			{
				std::cout << "You not have " << money << " money.\n";
			}
		}
		else
		{
			std::cout << "You can not transfer negative money!\n";
		}
	}
}