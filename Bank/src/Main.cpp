#pragma once

#include <src/Header.h>

#include "Account.h"

void CreateAccount(const char* userName, const char* password, const char* firstName, const char* lastName, Gender gender)
{
    Account::Create(userName, password, firstName, lastName, gender);
}

void ClearScreen()
{
    system("cls");
}

static int accountConnectedtIndex = -1;
static Account* accountConnected = nullptr;
void Connect();

int main()
{
    unsigned short action;
    do
    {
        std::cout << "Main Menu:\n"
            << "1 - Sign In\n"
            << "2 - Create Account\n"
            << "3 - Show Accounts\n"
            << "4 - Exit!\n";
        std::cout << "Select: ";
        std::cin >> action;

        ClearScreen();

        switch (action)
        {
            case 1:
            {
                accountConnectedtIndex = Account::SignIn();
                Connect();
            }
            break;
            case 2:
            {
                accountConnectedtIndex = Account::Create();
                Connect();
            }
            break;
            case 3:
            {
                Account::ShowList();
            }
            break;
            case 4: { } break;
            default:
            {
                std::cout << "Selcet from the menu!\n";
            }
            break;
        }
        std::cout << "\n";
    }
    while (action != 4);
}

void Connect()
{
    if (accountConnectedtIndex == -1)
    {
        return;
    }

    ClearScreen();

    accountConnected = &Account::s_accounts.at(accountConnectedtIndex);

    unsigned short action;
    do
    {
        std::cout << "Welcome " << accountConnected->m_user.GetFullName() << "\n";
        std::cout << "User Name: " << accountConnected->m_user.m_userName << "\n";
        std::cout << "Money: " << Account::GetMoneyAsText(accountConnected->m_money) << "\n";

        std::cout << "Actions Menu:\n"
            << "1 - Show account information\n"
            << "2 - Edit account information\n"
            << "3 - Change Password\n"
            << "4 - Deposit Money\n"
            << "5 - Withdraw Money\n"
            << "6 - Transfer Money\n"
            << "7 - Income and Expenses\n"
            << "8 - Discconect!\n";
        std::cout << "Select: ";
        std::cin >> action;

        ClearScreen();

        switch (action)
        {
            case 1:
            {
                accountConnected->ShowInformation();
            }
            break;
            case 2:
            {
                accountConnected->EditInformation();
            }
            break;
            case 3:
            {
                accountConnected->ChangePassword();
            }
            break;
            case 4:
            {
                accountConnected->DepositMoney();
            }
            break;
            case 5:
            {
                accountConnected->WithdrawMoney();
            }
            break;
            case 6:
            {
                accountConnected->TransferMoney(accountConnectedtIndex);
            }
            break;
            case 7:
            {
                Account::ShowTransferences(accountConnectedtIndex, accountConnected->m_lastIndexOfTransference);
            }
            break;
            case 8: { } break;
            default:
            {
                std::cout << "Selcet from the menu!\n";
            }
            break;
        }
        std::cout << "\n";
    } while (action != 8);
}